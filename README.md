
# .Net client library for the Qmuli Adjust InDesign File Production API.

## Adjust API

### API Endpointa & Documentation

All end-points can be found at [https://adjust2dev.qmuli.com/api-docs/index.html)

[This is the development site, contact Qmuli for API keys, and production access]

### Installation

```
Install-Package AdjustApiClient
```

### Usage

API Keys are available from Qmuli, please contact support for more information.

...  

## Issue Reporting

If you have found a bug or if you have a feature request, please report them at this repository issues section.
Please do not report security vulnerabilities on the public GitHub issue tracker. please disclose any security issues directly to us at support@qmuli.com

## Author

[QMuli Limited](www.qmuli.com)

## License

This client library project is licensed under the MIT license. See the LICENSE.txt file.
