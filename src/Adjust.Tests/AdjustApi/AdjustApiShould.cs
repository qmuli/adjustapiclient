﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Adjust.Tests.FileImport
{
    [TestClass]
    public class AdjustApiShould
    {
        [TestMethod]
        public async Task UploadAndListTemplates()
        {
            try
            {
                var settings = new Settings
                {
                    Username = TestSettings.AdjustApiUsername,
                    ApiKey = TestSettings.AdjustApiKey,
                    ApiBaseUrl = TestSettings.AdjustApiBaseUrl,
                    UseRetryLogic = TestSettings.AdjustApiUseRetryLogic,
                    RetryStatusCodes = TestSettings.AdjustApiRetryStatusCodes
                };

                var apiClient = new ApiClient(settings);

                var testIdmlInput = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "AdjustApi", "Files", "TestTemplate.idml");

                // Upload Template
                var templateId = await apiClient.Templates.Upload(testIdmlInput, new Api.Models.UploadParameters
                {
                    Description = $"Adjust.Tests.Upload {DateTime.Now}",
                    CallbackUrl = TestSettings.AdjustCallbackUrl
                });

                Assert.IsTrue(templateId > 0, $"Template Uploaded, Id is > 0, Template Id = {templateId}");

                // Get List of Templates and 
                var templates = await apiClient.Templates.GetList(new Api.Models.Templates.ListParameters());

                Assert.IsTrue(templates.List.Count > 0, "Template List > 0");

                Assert.IsTrue(templates.List.Exists(x => x.Id == templateId), $"Template Id = new template Id {templateId}");

                // Get template for production job, after processing
                // ---------------------------------------------------------
                // NOTE: This is not an efficient example for production use, use the callbacks to get the template job status and JSON data
                // ---------------------------------------------------------
                var template = templates.List.Where(x => x.Processed && x.ProcessingFailed == false && x.Id == templateId).ToList().FirstOrDefault();
                var counter = 0;
                while (template == null)
                {
                    if (counter > 5)
                    {
                        break;
                    }

                    System.Threading.Thread.Sleep(3000);

                    templates = await apiClient.Templates.GetList(new Api.Models.Templates.ListParameters { PageSize = int.MaxValue });
                    
                    template = templates.List.Where(x => x.Processed && x.ProcessingFailed == false && x.Id == templateId).ToList().FirstOrDefault();

                    counter++;
                }

                Assert.IsTrue(template != null, $"Template found, ready for production");

                // Test Get using template.id

                template = await apiClient.Templates.Get(template.Id);

                // Test JSON deserialize / seralize
                var jsonDataString = template.Data.ToJson();

                // Create Production Job
                var productionJobId = await apiClient.ProductionJobs.Create(new Api.Models.ProductionJobs.CreateJobParameters
                {
                    FileId = template.Id,
                    JsonData = jsonDataString,
                    ExportAsPdf = true,
                    CustomReference = "Adjust.Tests",
                    CallbackUrl = TestSettings.AdjustCallbackUrl
                });

                Assert.IsTrue(productionJobId > 0, $"Production Job Created, Id is > 0, Production Job Id = {productionJobId}");

                // Get List of Production Jobs
                var productionJobs = await apiClient.ProductionJobs.GetList(new Api.Models.ProductionJobs.ListParameters());

                // Get production job after processing
                // ---------------------------------------------------------
                // NOTE: This is not an efficient example for production use, use the callbacks to get the production job status and files
                // ---------------------------------------------------------
                var productionJob = productionJobs.List.Where(x => x.Processed && x.ProcessingFailed == false && x.Id == productionJobId).ToList().FirstOrDefault();
                counter = 0;
                while (productionJob == null)
                {
                    if (counter > 5)
                    {
                        break;
                    }

                    System.Threading.Thread.Sleep(4000);

                    productionJobs = await apiClient.ProductionJobs.GetList(new Api.Models.ProductionJobs.ListParameters());

                    productionJob = productionJobs.List.Where(x => x.Processed && x.ProcessingFailed == false && x.Id == productionJobId).ToList().FirstOrDefault();

                    counter++;
                }

                Assert.IsTrue(productionJob != null, $"Production Job completed");

                Assert.IsTrue(!string.IsNullOrWhiteSpace(productionJob.PdfUrl), $"Production Job PDF downloadble from: {productionJob.PdfUrl}");

                var archivedTemplateCount = await apiClient.Templates.SetArchived(new Api.Models.SetArchivedParameters { Archived = true, IdList = new List<int> { template.Id } });

                Assert.IsTrue(archivedTemplateCount > 0, $"Test template File archived");

                var archivedProductJobCount = await apiClient.ProductionJobs.SetArchived(new Api.Models.SetArchivedParameters { Archived = true, IdList = new List<int> { productionJob.Id } });

                Assert.IsTrue(archivedProductJobCount > 0, $"Test production job archived");
            }
            catch (Exception exception)
            {
                if (exception is ApiException)
                {
                    Assert.IsTrue(false, $"Api Exception: {((ApiException)exception).Message}");
                }

                throw;
            }
        }
    }
}
