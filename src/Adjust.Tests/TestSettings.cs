﻿using System;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;

namespace Adjust.Tests
{
    internal static class TestSettings
    {
        public static string AdjustApiUsername
        {
            get
            {
                var result = ConfigurationManager.AppSettings["AdjustApiUsername"];
                return String.IsNullOrWhiteSpace(result) ? "" : result;
            }
        }

        public static string AdjustApiKey
        {
            get
            {
                var result = ConfigurationManager.AppSettings["AdjustApiKey"];
                return String.IsNullOrWhiteSpace(result) ? "" : result;
            }
        }

        public static string AdjustApiBaseUrl
        {
            get
            {
                var result = ConfigurationManager.AppSettings["AdjustApiBaseUrl"];
                return String.IsNullOrWhiteSpace(result) ? "" : result;
            }
        }

        public static bool AdjustApiUseRetryLogic
        {
            get
            {
                if (String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["AdjustApiUseRetryLogic"]))
                {
                    return false;
                }

                return ConfigurationManager.AppSettings["AdjustApiUseRetryLogic"].Trim().ToLower() == "true";
            }
        }

        public static string AdjustApiRetryStatusCodes
        {
            get
            {
                var result = ConfigurationManager.AppSettings["AdjustApiRetryStatusCodes"];
                return String.IsNullOrWhiteSpace(result) ? "" : result;
            }
        }

        public static List<int> AdjustApiRetryStatusCodesList => AdjustApiRetryStatusCodes?.Split(new[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList() ?? new List<int>();

        public static string AdjustCallbackUrl
        {
            get
            {
                var result = ConfigurationManager.AppSettings["AdjustCallbackUrl"];
                return String.IsNullOrWhiteSpace(result) ? "" : result;
            }
        }
    }
}
