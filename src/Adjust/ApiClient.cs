﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Adjust.Api;
using Adjust.Core;

namespace Adjust
{
    public interface IApiClient
    {
        /// <summary>
        /// Gets an authentication token and initializes the API clients
        /// </summary>
        /// <returns></returns>
        void Initialize();

        /// <summary>
        /// Test endpoint to check if client authentication is valid
        /// </summary>
        /// <returns></returns>
        Task<bool> Authenticated();

        /// <summary>
        /// Templates API Client for endpoints relating to Templates
        /// </summary>
        ITemplates Templates { get; }
    }

    /// <summary>
    /// Adjust API Client Library
    /// </summary>
    public class ApiClient : IApiClient
    {
        /// <summary>
        /// Settings, passed in or loaded from appSettings
        /// </summary>
        private Settings ClientSettings { get; }

        /// <summary>
        /// HttpClient for making API calls
        /// </summary>
        private HttpClient HttpClient { get; set; }

        /// <inheritdoc/>
        public ITemplates Templates { get; private set; }

        /// <inheritdoc/>
        public IProductionJobs ProductionJobs { get; private set; }

        /// <inheritdoc/>
        public IContentFiles ContentFiles { get; private set; }

        /// <summary>
        /// Creates the Adjust API Client with the specified settings
        /// </summary>
        /// <param name="clientSettings"></param>
        /// <param name="token"></param>
        public ApiClient(Settings clientSettings)
        {
            ClientSettings = clientSettings;

            CheckSettings();

            if (!ClientSettings.ApiBaseUrl.EndsWith("/"))
            {
                ClientSettings.ApiBaseUrl = $"{ClientSettings.ApiBaseUrl}/";
            }

            Initialize();
        }

        private void CheckSettings()
        {
            if (string.IsNullOrWhiteSpace(ClientSettings.Username))
            {
                throw new Exception("Username setting not set");
            }

            if (string.IsNullOrWhiteSpace(ClientSettings.ApiKey))
            {
                throw new Exception("ApiKey setting not set");
            }

            if (string.IsNullOrWhiteSpace(ClientSettings.ApiBaseUrl))
            {
                throw new Exception("ApiBaseUrl setting not set");
            }

            if (ClientSettings.UseRetryLogic == true)
            {
                if (string.IsNullOrWhiteSpace(ClientSettings.RetryStatusCodes))
                {
                    throw new Exception("RetryStatusCodes not set");
                }

                try
                {
                    if (!ClientSettings.RetryStatusCodes.Any())
                    {
                        throw new Exception("RetryStatusCodes not set");
                    }
                }
                catch
                {
                    throw new Exception("RetryStatusCodes not valid");
                }
            }
        }

        /// <inheritdoc/>
        public void Initialize()
        {
            HttpClient = GetHttpClient();

            Templates = new Templates(HttpClient);
            ProductionJobs = new ProductionJobs(HttpClient);
            ContentFiles = new ContentFiles(HttpClient);
        }

        /// <inheritdoc/>
        public async Task<bool> Authenticated()
        {
            var response = await HttpClient.PostAsync($"{ClientSettings.ApiBaseUrl}Auth/Authenticated/", null);
            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Gets the Http Client for making the API calls
        /// </summary>
        /// <returns></returns>
        private HttpClient GetHttpClient()
        {
            HttpClient apiClient;

            if (ClientSettings.UseRetryLogic == true && ClientSettings.RetryStatusCodesList.Any())
            {
                apiClient = new HttpClient(new HttpClientRetryHandler(new HttpClientHandler(), ClientSettings.RetryStatusCodesList)) { BaseAddress = new Uri(ClientSettings.ApiBaseUrl) };
            }
            else
            {
                apiClient = new HttpClient { BaseAddress = new Uri(ClientSettings.ApiBaseUrl) };
            }

            apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            apiClient.DefaultRequestHeaders.Add("Username", ClientSettings.Username);
            apiClient.DefaultRequestHeaders.Add("ApiKey", ClientSettings.ApiKey);

            return apiClient;
        }
    }
}