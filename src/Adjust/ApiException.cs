﻿using System;
using System.Net;
using System.Security.Authentication;
using Adjust.Api.Models;
using Newtonsoft.Json;

namespace Adjust
{
    public class ApiException : Exception
    {
        public ApiException(string reference, HttpStatusCode statusCode, string responseContent)
        {
            switch (statusCode)
            {
                case HttpStatusCode.Unauthorized:
                {
                    throw new AuthenticationException();
                }
                case HttpStatusCode.BadRequest:
                {
                    var message = string.Empty;
                    try
                    {
                        var errorResult = JsonConvert.DeserializeObject<ErrorResult>(responseContent);
                        message = errorResult.Message;
                        throw new Exception($"{(int)statusCode}, {statusCode}, {reference} : {responseContent}");
                    }
                    catch
                    {
                        throw new Exception($"{(int)statusCode}, {statusCode}, {reference} : {message}");
                    }
                }
                default:
                {
                    throw new Exception($"{(int)statusCode}, {statusCode}, {reference} : {responseContent}");
                }
            }
        }
    }
}
