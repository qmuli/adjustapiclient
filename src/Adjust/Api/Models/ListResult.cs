﻿using System.Collections.Generic;

namespace Adjust.Api.Models
{
    public class ListResult<T>
    {
        public ListResult()
        {
            Paging = new ListPagingFilters();
        }

        public List<T> List { get; set; }

        public ListPagingFilters Paging { get; set; }

        public List<string> Errors { get; set; }
    }
}