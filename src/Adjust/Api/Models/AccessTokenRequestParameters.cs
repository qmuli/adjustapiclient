﻿using Newtonsoft.Json;

namespace Adjust.Api.Models
{
    public class AccessTokenRequestParameters
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("apiKey")]
        public string ApiKey { get; set; }
    }
}
