﻿using Newtonsoft.Json;
using System;

namespace Adjust.Api.Models.ProductionJobs
{
    public class ProductionJob
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("fileId")]
        public int FileId { get; set; }

        [JsonProperty("filename")]
        public string Filename { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("jsonData")]
        public string JsonData { get; set; }

        [JsonProperty("imageUrl")]
        public string ImageUrl { get; set; }

        [JsonProperty("pdfUrl")]
        public string PdfUrl { get; set; }

        [JsonProperty("inDesignFileUrl")]
        public string InDesignFileUrl { get; set; }

        [JsonProperty("inDesignTemplateUrl")]
        public string InDesignTemplateUrl { get; set; }

        [JsonProperty("processed")]
        public bool Processed { get; set; }

        [JsonProperty("processingFailed")]
        public bool ProcessingFailed { get; set; }

        [JsonProperty("processedDate")]
        public DateTimeOffset? ProcessedDate { get; set; }

        [JsonProperty("archived")]
        public bool Archived { get; set; }

        [JsonProperty("createdDate")]
        public DateTimeOffset CreateDate { get; set; }

        [JsonIgnore]
        public InDesignServer.VariableData.Template Data
        {
            get
            {
                if (string.IsNullOrEmpty(JsonData))
                {
                    return null;
                }

                try
                {
                    return JsonConvert.DeserializeObject<InDesignServer.VariableData.Template>(JsonData);
                }
                catch
                {
                    return null;
                }
            }
        }
    }
}