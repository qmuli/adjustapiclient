﻿using Newtonsoft.Json;

namespace Adjust.Api.Models.ProductionJobs
{
    /// <summary>
    /// Production Job List Request Parameters
    /// </summary>
    public class ListParameters
    {
        /// <summary>
        /// Production Job Request Parameters
        /// </summary>
        public ListParameters()
        {
            Query = string.Empty;
            Page = 1;
            PageSize = 20;
            SortBy = "Id";
            SortByDescending = true;
            Archived = false;
        }

        /// <summary>
        /// UserId [Optional] only used for admin access
        /// </summary>
        [JsonProperty("userId")]
        public int? UserId { get; set; }

        /// <summary>
        /// Query (text search on text fields)
        /// </summary>
        [JsonProperty("query")]
        public string Query { get; set; }

        /// <summary>
        /// Page, starting from 1
        /// </summary>
        [JsonProperty("page")]
        public int Page { get; set; }

        /// <summary>
        /// Maximum rows per page
        /// </summary>
        [JsonProperty("pageSize")]
        public int? PageSize { get; set; }

        /// <summary>
        /// Sorts list By
        /// Valid property names: Name, Id, LastUpdatedDate, CreatedDate
        /// </summary>
        [JsonProperty("sortBy")]
        public string SortBy { get; set; }

        /// <summary>
        /// Sort Order Direction
        /// </summary>
        [JsonProperty("sortByDescending")]
        public bool? SortByDescending { get; set; }

        /// <summary>
        /// Archived filter
        /// </summary>
        [JsonProperty("archived")]
        public bool? Archived { get; set; }
    }
}