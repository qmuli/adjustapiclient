﻿using Newtonsoft.Json;

namespace Adjust.Api.Models.ProductionJobs
{
    /// <summary>
    /// Create Production Job API Parameters
    /// </summary>
    public class CreateJobParameters
    {
        public CreateJobParameters()
        {
            UserId = 0;
            FileId = 0;
            JsonData = "";
            ExportAsImage = false;
            ExportAsPdf = false;
            ExportAsInDesignFile = false;
            ExportAsPdf = false;
            ExportAsInDesignFile = false;
            ExportAsInDesignTemplate = false;
            CustomReference = "";
            CallbackUrl = "";
        }

        /// <summary>
        /// UserId Filter [Optional, Admin Only, defaults to API user]
        /// </summary>
        [JsonProperty("userId")]
        public int? UserId { get; set; }

        /// <summary>
        /// Template File ID (Uploaded InDesign Template ID)
        /// </summary>
        [JsonProperty("fileId")]
        public int FileId { get; set; }

        /// <summary>
        /// JSON Data, Production Job JSON Data, updated as required for production
        /// </summary>
        [JsonProperty("jsonData")]
        public string JsonData { get; set; }

        /// <summary>
        /// Set to true to export output as image(s)
        /// </summary>
        [JsonProperty("exportAsImage")]
        public bool? ExportAsImage { get; set; }

        /// <summary>
        /// Set to true to export output as pdf
        /// </summary>
        [JsonProperty("exportAsPdf")]
        public bool? ExportAsPdf { get; set; }

        /// <summary>
        /// Set to true to export output as Indesign Template File (.indd)
        /// </summary>
        [JsonProperty("exportAsInDesignFile")]
        public bool? ExportAsInDesignFile { get; set; }

        /// <summary>
        /// Set to true to export output as Indesign Template File (.idml)
        /// </summary>
        [JsonProperty("exportAsInDesignTemplate")]
        public bool? ExportAsInDesignTemplate { get; set; }

        /// <summary>
        /// Custom / External Reference/ID, returned on callbacks
        /// </summary>
        [JsonProperty("customReference")]
        public string CustomReference { get; set; }

        /// <summary>
        /// Callback / Webhook URL, overrides api user configured callback
        /// </summary>
        [JsonProperty("callbackUrl")]
        public string CallbackUrl { get; set; }
    }
}