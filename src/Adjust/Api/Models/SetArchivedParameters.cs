﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Adjust.Api.Models
{
    /// <summary>
    /// Production Jop Archive Parameters
    /// </summary>
    public class SetArchivedParameters
    {
        /// <summary>
        /// Archived Set true to Archive, false to UnArchive
        /// </summary>
        [JsonProperty("archived")]
        public bool Archived { get; set; }

        /// <summary>
        /// List of IDs to archive
        /// </summary>
        [JsonProperty("idList")]
        public List<int> IdList { get; set; }
    }
}