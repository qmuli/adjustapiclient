﻿using System;
using Newtonsoft.Json;

namespace Adjust.Api.Models.Templates
{
    public class TemplateFile
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("fileName")]
        public string Filename { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("jsonData")]
        public string JsonData { get; set; }

        [JsonProperty("customReference")]
        public string CustomReference { get; set; }

        [JsonProperty("height")]
        public decimal? Height { get; set; }

        [JsonProperty("width")]
        public decimal? Width { get; set; }

        [JsonProperty("pageCount")]
        public int? PageCount { get; set; }

        [JsonProperty("processed")]
        public bool Processed { get; set; }

        [JsonProperty("processingFailed")]
        public bool ProcessingFailed { get; set; }

        [JsonProperty("archived")]
        public bool Archived { get; set; }

        [JsonProperty("imageUrl")]
        public string ImageUrl { get; set; }

        [JsonProperty("createdDate")]
        public DateTimeOffset CreatedDate { get; set; }

        [JsonIgnore]
        public InDesignServer.VariableData.Template Data
        {
            get
            {
                if (string.IsNullOrEmpty(JsonData))
                {
                    return null;
                }

                try
                {
                    return JsonConvert.DeserializeObject<InDesignServer.VariableData.Template>(JsonData);
                }
                catch
                {
                    return null;
                }
            }
        }
    }
}