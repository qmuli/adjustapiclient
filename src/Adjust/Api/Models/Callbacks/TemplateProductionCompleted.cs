﻿using Newtonsoft.Json;

namespace Adjust.Api.Models.Callbacks
{
    public class TemplateProductionCompleted
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("processedSuccessfully")]
        public bool ProcessedSuccessfully { get; set; }

        [JsonProperty("pdfUrl")]
        public string PdfUrl { get; set; }

        [JsonProperty("inDesignFileUrl")]
        public string InDesignFileUrl { get; set; }

        [JsonProperty("inDesignTemplateUrl")]
        public string InDesignTemplateUrl { get; set; }

        [JsonProperty("pageCount")]
        public int PageCount { get; set; }

        [JsonProperty("imageUrls")]
        public string ImageUrls { get; set; }

        [JsonProperty("customReference")]
        public string CustomReference { get; set; }

        /// <summary>
        /// Deserialize Production Job JSON Data from callback
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static TemplateProductionCompleted Deserialize(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                return null;
            }

            try
            {
                return JsonConvert.DeserializeObject<TemplateProductionCompleted>(json);
            }
            catch
            {
                return null;
            }
        }
    }
}