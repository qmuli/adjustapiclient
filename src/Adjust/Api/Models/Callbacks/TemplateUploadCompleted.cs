﻿using Newtonsoft.Json;

namespace Adjust.Api.Models.Callbacks
{
    public class TemplateUploadCompleted
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("fileId")]
        public int FileId { get; set; }

        [JsonProperty("jobId")]
        public int JobId { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("jsonData")]
        public string JsonData { get; set; }

        [JsonProperty("imageUrls")]
        public string ImageUrls { get; set; }

        [JsonProperty("processedSuccessfully")]
        public bool ProcessedSuccessfully { get; set; }

        [JsonProperty("customReference")]
        public string CustomReference { get; set; }

        [JsonIgnore]
        public InDesignServer.VariableData.Template Data
        {
            get
            {
                if (string.IsNullOrEmpty(JsonData))
                {
                    return null;
                }

                try
                {
                    return JsonConvert.DeserializeObject<InDesignServer.VariableData.Template>(JsonData);
                }
                catch
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Deserialize Template File JSON Data from callback
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static TemplateUploadCompleted Deserialize(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                return null;
            }

            try
            {
                return JsonConvert.DeserializeObject<TemplateUploadCompleted>(json);
            }
            catch
            {
                return null;
            }
        }
    }
}