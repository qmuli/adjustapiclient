﻿using Newtonsoft.Json;

namespace Adjust.Api.Models
{
    /// <summary>
    /// Upload Template API Parameters
    /// </summary>
    public class UploadParameters
    {
        /// <summary>
        /// Description (will default to filename if empty)
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Custom / External Reference/ID, returned on callbacks
        /// </summary>
        [JsonProperty("customReference")]
        public string CustomReference { get; set; }

        /// <summary>
        /// Callback / Webhook URL, overrides api user configured callback
        /// </summary>
        [JsonProperty("callbackUrl")]
        public string CallbackUrl { get; set; }
    }
}