﻿namespace Adjust.Api.Models
{
    public class ListPagingFilters
    {
        public int? TotalCount { get; set; }

        public int? Page { get; set; }

        public int? PageSize { get; set; }

        public string SortBy { get; set; }

        public bool? SortByDescending { get; set; }
    }
}
