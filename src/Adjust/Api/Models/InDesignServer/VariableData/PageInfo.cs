﻿using Newtonsoft.Json;

namespace Adjust.Api.Models.InDesignServer.VariableData
{
    public class PageInfo
    {
        [JsonProperty("number")]
        public int Number { get; set; }

        [JsonProperty("height")]
        public decimal Height { get; set; }

        [JsonProperty("width")]
        public decimal Width { get; set; }
    }
}
