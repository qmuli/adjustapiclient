﻿using Newtonsoft.Json;

namespace Adjust.Api.Models.InDesignServer.VariableData
{
    public class TemplateVariable
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("page")]
        public int Page { get; set; }

        [JsonProperty("tag")]
        public string Tag { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("replace")]
        public bool Replace { get; set; }

        [JsonProperty("autoSizeFont")]
        public bool? AutoSizeFont { get; set; }

        [JsonProperty("fontName")]
        public string FontName { get; set; }

        [JsonProperty("fontSize")]
        public float? FontSize { get; set; }

        [JsonProperty("minFontSize")]
        public float? MinimumFontSize { get; set; }

        [JsonProperty("maxFontSize")]
        public float? MaximumFontSize { get; set; }

        [JsonProperty("minFontSizeDecreaseLimit")]
        public float? MinimumFontSizeDecreaseLimit { get; set; }

        [JsonProperty("maxFontSizeIncreaseLimit")]
        public float? MaxFontSizeIncreaseLimit { get; set; }
    }
}
