﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Adjust.Core;

namespace Adjust.Api.Models.InDesignServer.VariableData
{
    public class Template
    {
        [JsonProperty("version")]
        public int Version { get; set; }

        [JsonProperty("inputPath")]
        public string InputPath { get; set; }

        [JsonProperty("outputPath")]
        public string OutputPath { get; set; }

        [JsonProperty("logFilePath")]
        public string LogFilePath { get; set; }

        [JsonProperty("outputTypes")]
        public string OutputTypes { get; set; }

        [JsonProperty("height")]
        public decimal Height { get; set; }

        [JsonProperty("width")]
        public decimal Width { get; set; }

        [JsonProperty("pages")]
        public List<PageInfo> Pages { get; set; }

        [JsonProperty("units")]
        public string Units { get; set; }

        [JsonProperty("pageCount")]
        public int PageCount { get; set; }

        /// <summary>
        /// Global Minimum Font size for Text Adjument (for non tagged text frames)
        /// </summary>
        [JsonProperty("minFontSize")]
        public float MinimumFontSize { get; set; } = 6;

        /// <summary>
        /// Global Maximum Font size for Text Adjument (for non tagged text frames)
        /// </summary>
        [JsonProperty("maxFontSize")]
        public float MaximumFontSize { get; set; } = 600;

        [JsonProperty("variables")]
        public List<TemplateVariable> Variables { get; set; }

        public string ToJson()
        {
            return ObjectExtensions.ToJson(this);
        }
    }
}
