﻿
namespace Adjust.Api.Models.ContentFiles
{
    public class DeleteParameters
    {
        public int? UserId { get; set; }

        public string Filename { get; set; }
    }
}