﻿
namespace Adjust.Api.Models.ContentFiles
{
    /// <summary>
    /// Template List Request Parameters
    /// </summary>
    public class ListParameters
    {
        public int? UserId { get; set; }
    }
}