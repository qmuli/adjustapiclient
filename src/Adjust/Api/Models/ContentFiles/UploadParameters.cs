﻿using Newtonsoft.Json;

namespace Adjust.Api.Models.ContentFiles
{
    public class UploadParameters
    {
        /// <summary>
        /// User Id to associate content file (admin use only)
        /// </summary>
        [JsonProperty("userId")]
        public int? UserId { get; set; }
    }
}