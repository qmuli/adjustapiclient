﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Adjust.Core;
using Adjust.Api.Models.ContentFiles;

namespace Adjust.Api
{
    public interface IContentFiles
    {
        /// <summary>
        /// Gets the List of cached content files (list of filenames)
        /// </summary>
        Task<List<string>> GetList(ListParameters parameters = null);

        /// <summary>
        /// Uploads a new content file (.png, .jpg, .jpeg, .gif, .tif, .tiff. .bmp, .pdf)
        /// </summary>
        /// <param name="filePath">Path to file on disk</param>
        /// <param name="parameters">Upload parameters [Optional]</param>
        /// <returns>Returns the Template File Id</returns>
        Task<bool> Upload(string filePath, UploadParameters parameters = null);

        /// <summary>
        /// Uploads a new content file (.png, .jpg, .jpeg, .gif, .tif, .tiff. .bmp, .pdf)
        /// </summary>
        /// <param name="filename">Filename (.idml)</param>
        /// <param name="fileContent">File contens as byte array</param>
        /// <param name="parameters">Upload parameters [Optional]</param>
        /// <returns>Returns true if uploaded successfully, exception if file is not uploaded correctly</returns>
        Task<bool> Upload(string filename, byte[] fileContent, UploadParameters parameters = null);
    }

    public class ContentFiles : IContentFiles
    {
        public HttpClient ApiClient { get; set; }

        public ContentFiles(HttpClient apiClient)
        {
            ApiClient = apiClient;
        }

        /// <inheritdoc/>
        public async Task<List<string>> GetList(ListParameters parameters = null)
        {
            var response = await ApiClient.PostAsJsonAsync("Content/GetList/", parameters);

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsJsonAsync<List<string>>();
            }

            throw new ApiException("ContentFiles.GetList", response.StatusCode, await response.Content.ReadAsStringAsync());
        }

        /// <inheritdoc/>
        public async Task<bool> Upload(string filePath, UploadParameters parameters)
        {
            var filename = Path.GetFileName(filePath);

            var fileContent = File.ReadAllBytes(filePath);

            return await Upload(filename, fileContent, parameters);
        }

        /// <inheritdoc/>
        public async Task<bool> Upload(string filename, byte[] fileContent, UploadParameters parameters)
        {
            using (MultipartFormDataContent content = new MultipartFormDataContent())
            { 
                content.Add(new StreamContent(new MemoryStream(fileContent)), "file", WebUtility.UrlEncode(filename));

                var values = new[]
                {
                    new KeyValuePair<string, string>("userId", parameters.UserId.HasValue ? parameters.UserId.Value.ToString() : "")
                };

                foreach (var keyValuePair in values)
                {
                    content.Add(new StringContent(keyValuePair.Value), $"\"{keyValuePair.Key}\"");
                }

                var response = await ApiClient.PostAsync("Content/Upload/", content);

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }

                throw new ApiException("ContentFiles.Upload", response.StatusCode, await response.Content.ReadAsStringAsync());
            }
        }

        /// <inheritdoc/>
        public async Task<bool> Delete(DeleteParameters parameters)
        {
            var response = await ApiClient.PostAsJsonAsync("Content/Delete/", parameters);

            if (!response.IsSuccessStatusCode)
            {
                return true;
            }

            throw new ApiException("ContentFiles.Delete", response.StatusCode, await response.Content.ReadAsStringAsync());
        }
    }
}