﻿using System.Net.Http;
using System.Threading.Tasks;
using Adjust.Core;
using Adjust.Api.Models;
using Adjust.Api.Models.ProductionJobs;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Text;

namespace Adjust.Api
{
    public interface IProductionJobs
    {
        /// <summary>
        /// Gets the Production Job details by the specified Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ProductionJob> Get(int id);

        /// <summary>
        /// Gets the List of Production Jobs (paged, ordered and filtered)
        /// </summary>
        Task<ListResult<ProductionJob>> GetList(ListParameters parameters);

        Task<int> Create(CreateJobParameters parameters);

        /// <summary>
        /// Sets the specified list of production job IDs to Archived (true or false)
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>Returns the number of production jobs updated</returns>
        /// <exception cref="ApiException"></exception>
        Task<int> SetArchived(SetArchivedParameters parameters);
    }

    public class ProductionJobs : IProductionJobs
    {
        public HttpClient ApiClient { get; set; }

        public ProductionJobs(HttpClient apiClient)
        {
            ApiClient = apiClient;
        }

        /// <inheritdoc/>
        public async Task<ProductionJob> Get(int id)
        {
            var response = await ApiClient.GetAsync("Production/Get/{id}");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsJsonAsync<ProductionJob>();
            }

            throw new ApiException("ProductionJobs.Get", response.StatusCode, await response.Content.ReadAsStringAsync());
        }

        /// <inheritdoc/>
        public async Task<ListResult<ProductionJob>> GetList(ListParameters parameters)
        {
            var response = await ApiClient.PostAsJsonAsync("Production/GetList/", parameters);

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsJsonAsync<ListResult<ProductionJob>>();
            }

            throw new ApiException("ProductionJobs.GetList", response.StatusCode, await response.Content.ReadAsStringAsync());
        }

        /// <inheritdoc/>
        public async Task<int> Create(CreateJobParameters parameters)
        {
            //var parameterDictionary = new Dictionary<string, string>
            //{
            //    { "UserId", (parameters.UserId ?? 0).ToString() },
            //    { "FileId", parameters.FileId.ToString() },
            //    { "ExportAsImage", (parameters.ExportAsImage ?? false).ToString() },
            //    { "ExportAsPdf", (parameters.ExportAsImage ?? false).ToString() },
            //    { "ExportAsInDesignFile", (parameters.ExportAsImage ?? false).ToString() },
            //    { "ExportAsInDesignTemplate", (parameters.ExportAsImage ?? false).ToString() },
            //    { "CustomReference", parameters.CustomReference },
            //    { "CallbackUrl", parameters.CallbackUrl },
            //    { "JsonData", parameters.JsonData }
            //};
            //string strPayload = JsonConvert.SerializeObject(parameterDictionary);
            //HttpContent content = new StringContent(strPayload, Encoding.UTF8, "application/x-www-url-formencoded");
            //var response = await ApiClient.PostAsync("Production/CreateJob/", content);

            var response = await ApiClient.PostAsJsonAsync("Production/Create/", parameters);

            if (response.IsSuccessStatusCode)
            {
                var jsonStringResult = await response.Content.ReadAsStringAsync();

                var result = JObject.Parse(jsonStringResult);

                var id = result.Value<int>("id");

                return id;
            }

            throw new ApiException("ProductionJobs.CreateJob", response.StatusCode, await response.Content.ReadAsStringAsync());
        }

        /// <inheritdoc/>
        public async Task<int> SetArchived(SetArchivedParameters parameters)
        {
            var response = await ApiClient.PostAsJsonAsync("Production/SetArchived/", parameters);

            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync();

                return int.TryParse(result, out int count) ? count : 0;
            }

            throw new ApiException("ProductionJobs.SetArchived", response.StatusCode, await response.Content.ReadAsStringAsync());
        }
    }
}