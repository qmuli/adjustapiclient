﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Adjust.Core;
using Adjust.Api.Models;
using Adjust.Api.Models.Templates;
using Newtonsoft.Json.Linq;

namespace Adjust.Api
{
    public interface ITemplates
    {
        /// <summary>
        /// Gets the Template File details by the specified Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<TemplateFile> Get(int id);

        /// <summary>
        /// Gets the List of templates (paged, ordered and filtered)
        /// </summary>
        Task<ListResult<TemplateFile>> GetList(ListParameters parameters);

        /// <summary>
        /// Uploads a new Template File (.idml)
        /// </summary>
        /// <param name="filePath">Path to file on disk</param>
        /// <param name="parameters">Upload parameters</param>
        /// <returns>Returns the Template File Id</returns>
        Task<int> Upload(string filePath, UploadParameters parameters);

        /// <summary>
        /// Uploads a new Template File (.idml)
        /// </summary>
        /// <param name="filename">Filename (.idml)</param>
        /// <param name="fileContent">File contens as byte array</param>
        /// <param name="parameters">Upload parameters</param>
        /// <returns>Returns the Template File Id</returns>
        Task<int> Upload(string filename, byte[] fileContent, UploadParameters parameters);

        /// <summary>
        /// Sets the specified list of template IDs to Archived (true or false)
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>Returns the number of templates updated</returns>
        /// <exception cref="ApiException"></exception>
        Task<int> SetArchived(SetArchivedParameters parameters);
    }

    public class Templates : ITemplates
    {
        public HttpClient ApiClient { get; set; }

        public Templates(HttpClient apiClient)
        {
            ApiClient = apiClient;
        }

        /// <inheritdoc/>
        public async Task<TemplateFile> Get(int id)
        {
            var response = await ApiClient.GetAsync($"Templates/Get/?id={id}");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsJsonAsync<TemplateFile>();
            }

            throw new ApiException("Templates.Get", response.StatusCode, await response.Content.ReadAsStringAsync());
        }

        /// <inheritdoc/>
        public async Task<ListResult<TemplateFile>> GetList(ListParameters parameters)
        {
            var response = await ApiClient.PostAsJsonAsync("Templates/GetList/", parameters);

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsJsonAsync<ListResult<TemplateFile>>();
            }

            throw new ApiException("Templates.GetList", response.StatusCode, await response.Content.ReadAsStringAsync());
        }

        /// <inheritdoc/>
        public async Task<int> Upload(string filePath, UploadParameters parameters)
        {
            var filename = Path.GetFileName(filePath);

            var fileContent = File.ReadAllBytes(filePath);

            return await Upload(filename, fileContent, parameters);
        }

        /// <inheritdoc/>
        public async Task<int> Upload(string filename, byte[] fileContent, UploadParameters parameters)
        {
            using (MultipartFormDataContent content = new MultipartFormDataContent())
            {
                content.Add(new StreamContent(new MemoryStream(fileContent)), "file", WebUtility.UrlEncode(filename));

                if (parameters != null)
                {
                    var values = new[]
                    {
                        new KeyValuePair<string, string>("description", parameters.Description ?? ""),
                        new KeyValuePair<string, string>("customReference", parameters.CustomReference ?? ""),
                        new KeyValuePair<string, string>("callbackUrl", parameters.CallbackUrl ?? "")
                    };

                    foreach (var keyValuePair in values)
                    {
                        content.Add(new StringContent(keyValuePair.Value), $"\"{keyValuePair.Key}\"");
                    }
                }

                var response = await ApiClient.PostAsync("Templates/Upload/", content);

                if (response.IsSuccessStatusCode)
                {
                    var jsonStringResult = await response.Content.ReadAsStringAsync();

                    var result = JObject.Parse(jsonStringResult);

                    var id = result.Value<int>("id");

                    return id;
                }

                throw new ApiException("Templates.Upload", response.StatusCode, await response.Content.ReadAsStringAsync());
            }
        }

        /// <inheritdoc/>
        public async Task<int> SetArchived(SetArchivedParameters parameters)
        {
            var response = await ApiClient.PostAsJsonAsync("Templates/SetArchived/", parameters);

            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync();

                return int.TryParse(result, out int count) ? count : 0;
            }

            throw new ApiException("Approvals.SetActive", response.StatusCode, await response.Content.ReadAsStringAsync());
        }
    }
}