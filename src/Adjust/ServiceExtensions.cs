﻿using Adjust.Api;
using Microsoft.Extensions.DependencyInjection;

namespace Adjust
{
    public static class ServicesExtensions
    {
        public static void AddAdjustApi(this IServiceCollection services)
        {
            services.AddScoped<ITemplates, Templates>();
            services.AddScoped<IApiClient, ApiClient>();
        }
    }
}
