﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Adjust
{
    public class Settings
    {
        /// <summary>
        /// Adfast Username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Adfast API Key
        /// </summary>
        public string ApiKey { get; set; }

        /// <summary>
        /// Adjust Base Url for all API Calls
        /// </summary>
        public string ApiBaseUrl { get; set; }

        /// <summary>
        /// UseRetryLogic to retry api calls if the status code is present in RetryStatusCodes
        /// </summary>
        public bool? UseRetryLogic { get; set; }

        /// <summary>
        /// Retry logic will be applied to the RetryStatusCodes supplied. (command separated string)
        /// </summary>
        public string RetryStatusCodes { get; set; }

        /// <summary>
        /// RetryStatusCodes as List
        /// </summary>
        public List<int> RetryStatusCodesList => RetryStatusCodes?.Split(new[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList() ?? new List<int>();
    }
}